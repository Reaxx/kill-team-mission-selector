// Toggles the elements display between block and hide
function ToggleVisibility(elementId) {
    var element = document.getElementById(elementId);
    console.log(element.style.display);    
 
    // If hidden, show. Else hide.
    if(element.style.display != "block") {
        element.style.display = "block";
    }
    else {  
        element.style.display = "none";
    }
}