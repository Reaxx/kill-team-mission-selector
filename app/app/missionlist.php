<?php

class MissionList
{

    private $DB;
    private $Table;
    public $Missions;

    function __construct()
    {
    }

    // function Headers()
    // {
    //     $data = get_object_vars($this->Missions[0]);
    //     foreach ($data as $key => $value) {
    //         $headers[] = $key;
    //     }

    //     return $headers;
    // }

    function GetMissionsAsJSON()
    {
        return json_encode($this->Missions);
    }

    /**
     * FormatFilterWithWhitelist
     * Checks the filter against a given whitelist, remvoing and creating an error messege for all not found in the whitelist.
     * @param  array $filters Filter
     * @param  array(string) $whitelist Whitelist
     *
     * @return array First element coitans whitelisted entries, second errormessages.
     */
    static function FormatFilterWithWhitelist(array $filters, array $whitelist): array
    {
        $filter = array();

        foreach ($filters as $key => $value) {
            if (in_array($key, $whitelist)) {
                $filter[$key] = $value;
            } else {
                $msg[] = "Key '" . $key . "' not found or not allowed.";
            }
        }

        return array($filter, $msg);
    }

    static function FormatFilterWithBlacklist(array $filters, array $blacklist): array
    {
        $filter = array();

        foreach ($filters as $key => $value) {
            if (!in_array($key, $blacklist)) {
                $type = ucfirst($key);
                $filter[$type] = $value;
            } else {
                $msg[] = "Key '" . $key . "' not found or not allowed.";
            }
        }

        return array($filter, $msg);
    }

    public function LoadAll()
    {
        //Returns cached allPages if found
        if (!empty($this->Missions)) {
            return $this->Missions;
        }

        //Setts databaseconnection if not existing
        if (!$this->DB) {
            $this->SetDB();
        }

        $sql = 'SELECT * FROM ' . $this->Table;

        //Result from db
        $result = $this->DB->exec($sql);


        //If no results or table not found
        if (!$result) {
            return false;
        }

        //Parses missions into objects
        foreach ($result as $mission) {
            $m = new Mission();
            $m->ParseMissionFromAssoc($mission);
            $this->Missions[] = $m;
        }

        return $this->Missions;
    }

    /**
     * GetMissionsByFilter
     *
     * @param  mixed $filter array property=>value pairs
     *
     * @return array array of hits
     */
    public function GetMissionsByFilter(array $filter): array
    {
        //Starts with all missions
        $foundMissons = $this->Missions;

        //Overwrites with 
        foreach ($filter as $key => $value) {
            $foundMissons = $this->FilterMissionsByProp($key, (array) $value, (array) $foundMissons);
        }

        return $foundMissons;
    }


    /**
     * FilterMissionsByProp
     * Filters missions based on a given property
     * @param  mixed $prop Property to filter on
     * @param  mixed $needles Keywords to find in given proprty
     * @param  mixed $missions Missions to run filter on
     *
     * @return array Array of unique mission tat match the neddles
     */
    private function FilterMissionsByProp(string $prop, array $needles, array $missions)
    {

        $hits = array();

        foreach ($missions as $mission) {
            //Extra iteration to handle comma-seperated values.
            foreach (self::ExplodeAndJoin((array) $mission->$prop, ',') as $sProp) {
                //Forcing value to string to compare bools
                if (in_array($sProp, $needles)) {
                    $hits[] = $mission;
                }
            }
        }

        // echo count($missions);
        // return array_unique($hits,SORT_REGULAR);
        return array_unique($hits, SORT_REGULAR);
    }

    private function FormatDescriptionsForFilterList($descriptionArray) {

    }


    public function AddMissions(array $missions)
    {
        $this->Missions = array_merge((array) $this->Missions, $missions);
    }

    private function ParseMissions(array $dbData)
    {
        foreach ($dbData as $row) {
            $m = new Mission();
            foreach ($row as $name => $data) {

                $m->$name = $data;
            }

            $this->Missions[] = $m;
        }
    }

    function GetRandomMission()
    {
        $max = count((array) $this->Missions) - 1;
        $randN = rand(0, $max);

        return $this->Missions[$randN];
    }

    //Helperfunction, should proberbly be movec to another class
    /**
     * ExplodeAndJoin
     * Explodes the elements of a given array on seperator, and rejoins the array.
     * @param  mixed $array Array to format
     * @param  mixed $seperator String to explode the element on
     *
     * @return array New array
     */
    static function ExplodeAndJoin(array $array, string $seperator): array
    {
        $resultArray = array();

        //Iterates over, spltting on commas and adding to new array
        foreach ($array as $preEElement) {
            foreach (explode($seperator, $preEElement) as $postEElement) {
                $resultArray[] = $postEElement;
            }
        }

        //Removing nulls
        $resultArray = array_filter($resultArray);
        sort($resultArray);

        //Returns uniquie
        return array_unique($resultArray, SORT_REGULAR);
    }


    /**
     * GetUniqueProperties
     * Might give unexpcted begavior for Campaigns, if so check GetUniqueCampagins()
     * @param  mixed $property
     *
     * @return array
     */
    function GetUniqueProperties(string $property)
    {
        $columns = array_column((array) $this->Missions, $property);
        $unique = array_unique($columns);
        //Removing nulls
        $unique = array_filter($unique);
        sort($unique);
        return $unique;
    }

    function GetProperties(array $properties)
    {
        // $list 
        return array_column((array) $this->Missions, $properties[0]);
    }
    // private function GetPropertiy(string $propertie)

    function GetPoints()
    {
    }

    private function SetDB()
    {
        //Makes sure that ParseIni is set
        ParseIni::SetIni("./app/app/config/config.ini");

        //Loads database and table name
        $dbPath = ParseIni::Get("config", "DBPath");
        $this->Table = ParseIni::Get("config", "missonDBTableName");

        //Creates DB ORM        
        $this->DB = new DB\SQL("sqlite:" . $dbPath);
    }
}
