<?php

class Mission extends DBitems
{
    public $Id;
    public $Name;
    public $Type;
    public $Points;
    public $ScoutPhase;
    public $Commander;
    public $Players;
    public $Description;
    public $Source;
    public $Page;
    public $Campaigns;
    
    function SetTestData()
    {        
        $this->Name = Helper::GenerateTestString(1,20,false);
		$this->Type = Helper::GenerateTestString(1,20,false);
		$this->Points = rand(0,200);
		$this->ScoutPhase = (bool) rand(0,1);
		$this->Commander = (bool) rand(0,1);
		$this->Players = rand(2,4);
		$this->Description = Helper::GenerateTestString(1,50,false);
		$this->Source = Helper::GenerateTestString(1,20,false);
		$this->Page = Helper::GenerateTestString(1,20,false);
    }

    //Sets $this->db
    protected function SetDbPath()
    {
		ParseIni::SetIni("./app/app/config/config.ini");
        $this->dbPath = ParseIni::Get("config","DBPath");        
	}
	//Sets $this->mapper
	function SetTableName() {
        $this->tableName = ParseIni::Get("config","missonDBTableName");
    }
	//Sets $this->startSql
	function SetStartSql() {
        $this->schemaSql =
			'CREATE TABLE IF NOT EXISTS "' . $this->tableName . '" (
            `Id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
            `Name` TEXT NOT NULL,
			`Type` TEXT NOT NULL, 
			`Points` TEXT,
            `ScoutPhase` TEXT NOT NULL,
            `Commander` TEXT NOT NULL,
            `Players` TEXT NOT NULL,
            `Description` TEXT,
            `Source` TEXT NOT NULL,
            `Page` TEXT NOT NULL,
            `Campaigns` TEXT
			)';
    }
	//Sets the object
	function SetData(...$args) {
        
    }

    function GetCampaigns()
    {
        return explode(",",$this->Campaigns);
    }

	function Load() {
        // if($this->Id === null) {
        //     throw new Exception("Id can not be null");            
        // }

        // $data = $this->LoadByField("Id", $this->Id);
        // $this->ParseMissionFromDB($data);
    }
    function Delete() : bool {
        if($this->Id === null) {
            throw new Exception("Id can not be null");            
        }

        $this->LoadByField("Id", $this->Id);
        $this->mapper->erase();

        return true;
    }
	function Save() {
        //Checks that the obeject has valid form
        if(($msg = $this->CheckIfValid()) !== true) {
            throw new Exception(implode($msg));       
        }

        //If Id is given, loads thru the mapper so that it knows to overwrite it
        if($this->Id != null)
        {
            $this->LoadByField("Id", $this->Id);
        }    

        $this->mapper->copyfrom($this);

        //If data is unchanged aborts writing to database
        if(!$this->mapper->changed()) {
            return false;
        }

        if(!$this->mapper->save()) {
				throw new Exception("Writing to database failed");
            }
            
            return true;
    }

    private function CheckIfValid() {
        //Fields to ignore (bools and specials)
        $ignore = array("Id","Commander","ScoutPhase");

        $msg = array();
        foreach ($this as $key => $value) {
            //If property is marked as not null
            if($this->mapper->required($key) && $value == null && !in_array($key,$ignore))
            {
                $msg[] = $key." is null.";
            }
        }

        //IF no error-message was found, return true
        if(!$msg) {
            return true;
        }

        //Otherwise return array of error messages
        return $msg;
    }

    /**
     * ParseMissionFromAssoc
     * Populate object from an AssocArray
     * @param  mixed $data AssocArray (or object) with Property=>Value
     *
     * @return bool
     */
    public function ParseMissionFromAssoc($data)
    {
        foreach ($data as $key => $value) {
            $this->$key = strip_tags($value);
        }

        return true;
    }


}
