<?php
class MissionController
{
	//Runs before other classes
	function beforeroute($f3)
	{ }

	function Index($f3)
	{
		$mDB = new MissionList();
		//Supressing error if databaseload fails		
		@$mDB->LoadAll();

		$m = new MissionList();

		if ($f3["GET"]) {
			$filter = array();
			//Fields not to filter on
			$ignore = array("showall");

			$filter = MissionList::FormatFilterWithBlacklist($f3["GET"],$ignore)[0];
			$filteredMissions = $mDB->GetMissionsByFilter($filter);
			$m->AddMissions($filteredMissions);
		} else {
			$m = $mDB;
		}

		//Showall setts flag for showing all hits
		$f3->set('showAll', $f3["GET"]["showall"]);

		//Setts the order that the filter-options should be generated
		$filterOrder = array ("Type","Points","Description","Players","ScoutPhase","Commander","Campaigns","Source");
		//Setts the order that the values of the mission should be shown
		$showOrder = array ("Name","Type","Points","Description","Players","ScoutPhase","Commander","Campaigns","Source","Page");

		//Setting variables for UI
		$f3->set('selectedMission', $m->GetRandomMission());
		$f3->set('missions', $m);
		$f3->set('missionsDB', $mDB);
		$f3->set('GET', $f3["GET"]);
		$f3->set('filterOrder',$filterOrder);
		$f3->set('showOrder',$showOrder);

		foreach($filterOrder as $property) {
			$f3->set("countedProperty[$property]",$this->CountAndFormatChoices($f3["GET"][$property],$property));
		}

		echo Template::instance()->render('mainpage.html');
	}
	
	private function CountAndFormatChoices($list) {
		$n = count((array) $list);

		//Returns a formated list, if no results found results false;
		if($n === 0) {
			false;
		} else {
			$formatedList = implode(", ",$list);
			return "($n: $formatedList)";
		}
	}


	function ShowAdd($f3, $params)
	{
		//Loading all missions from the database
		$ml = new MissionList();
		@$ml->LoadAll();

		//IF ID given, retrives the mission
		if ($params["id"]) {
			$filter = array("Id" => $params["id"]);

			//Id is unique so should get only one hit.
			$activeMission = $ml->GetMissionsByFilter($filter)[0];
			$f3->set('activeMission', $activeMission);
		}

		$f3->set('missions', $ml);
		echo Template::instance()->render('addmission.html');
	}

	function AddMission($f3, $params)
	{
		$m = new Mission();
		$m->ParseMissionFromAssoc($f3["POST"]);
		if ($m->Save()) {
			echo "Mission " . $m->Name . " sucessfuly created";
		} else {
			echo "Misson failed to create, please contact admin";
		}

		//Redraws add-screen
		echo "<hr>";
		$this->ShowAdd($f3, $params);
	}

	function UpdateMission($f3, $params)
	{
		$m = new Mission();
		$m->Id = $params["id"];

		$m->ParseMissionFromAssoc($f3["POST"]);

		if ($m->Save()) {
			echo "Mission " . $m->Name . " sucessfuly updated";
		} else {
			echo "Misson update faild, please contact admin";
		}

		//Redraws add-screen
		echo "<hr>";
		$this->ShowAdd($f3, $params);
	}

	function DeleteMission($f3, $params)
	{
		$m = new Mission();
		$m->Id = $params["id"];

		if ($m->Delete()) {
			echo "Mission " . $m->Name . " sucessfuly deleated";
		} else {
			echo "Misson failed to deleate, please contact admin";
		}

		//Redraws add-screen
		echo "<hr>";
		$this->ShowAdd($f3, $params);
	}

	function AddTest($f3, $params)
	{
		$q = $params["repeat"];

		for ($i = 1; $i <= $q; $i++) {
			$mission = new Mission();
			$mission->SetTestData();

			if ($mission->Save()) {
				echo "Mission generated (" . $i . ")<br>";
			}
		}
	}

	function ShowMissionsAsJSON($f3, $params)
	{
		//List of the avalibe variables
		$whitelist = array("Id", "Name","Type","Points","Commander","Players","Source","Description","Page","Campaigns");

		$ml = new MissionList();
		$ml->LoadAll();

		if ($f3["GET"]) {
			$result = MissionList::FormatFilterWithWhitelist($f3["GET"],$whitelist);
			$msg = $result[1];
			$tmpMl = array();

			if($result[0] != null) {
				$tmpMl = $ml->GetMissionsByFilter($result[0]);
			}			

			$ml->Missions = $tmpMl;		
		}


		echo json_encode(array("missions"=>$ml->Missions,"message"=>$msg));
	}

	//Runs after other classes
	function afterroute($f3)
	{ }
}
