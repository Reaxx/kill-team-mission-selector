<?php


/**
* Helper 2.0
* Collection of useful methods
*/
class Helper
{
	/**
	* Parses a string into a url-safe string. Useful for tokens in F3.
	*/
	public static function SafeUrlString(string $string) : string
	{
		$safe = preg_replace('/^-+|-+$/', '', strtolower(preg_replace('/[^a-zA-Z0-9]+/', '-', $string)));
		return $safe;
	}


	/**
	 * GenerateTestString
	 * Returns a randomised string 
	 * @param  int $min Lowest number of characters (default 1)
	 * @param  int $max Higest number of characters (defaul 20)
	 * @param  bool $includeSpecialChars Include (some) special characters (reginal letters like å and some special symbols like ')
	 *
	 * @return string Randomised string
	 */
	public static function GenerateTestString(int $min = 1, int $max = 20, bool $includeSpecialChars = true) : string
    {
		$rand = rand($min, $max);
        $charset = "0123456789abcdefghijklmnopqrstuvwxyz";

        if($includeSpecialChars)
        {			
			$specialChars = "å";
            $charset .= $specialChars;            
        }

        return substr(str_shuffle(str_repeat($charset, $rand)),0,$rand);
    }

}
?>
